import type { Plugin } from 'vite';
import {execSync} from "child_process"
let envInjectionFailed = false;

const createPlugin = (): Plugin => {
  return {
    name: 'vite-plugin-package-version',
    config: async (_, env) => {
      if (env) {
        return {
          define: {
            ['import.meta.env.TPU_VERSION']: JSON.stringify(process.env.npm_package_version),
            ['import.meta.env.TPU_BUILD_DATE']: JSON.stringify(new Date().toISOString()),
            ['import.meta.env.TPU_COMMIT']: "deez"
          }
        };
      } else {
        envInjectionFailed = true;
      }
    },
    configResolved(config) {
      if (envInjectionFailed) {
        config.logger.warn(
          `[vite-plugin-package-version] import.meta.env.PACKAGE_VERSION was not injected due ` +
          `to incompatible vite version (requires vite@^2.0.0-beta.69).`
        );
      }
    },
  };
};

export default createPlugin;
